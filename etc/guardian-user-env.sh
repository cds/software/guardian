GUARDIAN_LOCATION=___GUARDIAN_LOCATION___
prepend () {
    path="$1"
    target="$2"
    current="$3"
    if [ -z "${current}" ] ; then
	export ${target}=${path}
    else
	if ! [[ "$current" =~ "$path" ]] ; then
	    export ${target}=${path}:${current}
	fi
    fi
}
prepend "${GUARDIAN_LOCATION}/bin" PATH "${PATH}"
prepend "${GUARDIAN_LOCATION}/lib/python2.7/site-packages" PYTHONPATH "${PYTHONPATH}"
