# -*- makefile -*-

PACKAGE = guardian

SVERS := $(shell git describe | sed -e 's/_/~/' -e 's/-/+/' -e 's/-/~/' -e 's|.*/||')
VFILE := lib/guardian/_version.py
CVERS := $(shell test -e $(VFILE) && PYTHONPATH=lib python3 -m guardian.version)
DIST := $(PACKAGE)-$(SVERS)

WITH_SYSTEMD := $(shell which systemd-run)
ifdef WITH_SYSTEMD
  TEST_NAME := guardtest_$(shell date | md5sum | cut -b -7)
  TEST_EXEC := systemd-run --user --scope --unit=$(TEST_NAME) --slice=$(TEST_NAME) #--collect
  TEST_CLEAN := systemctl --user stop $(TEST_NAME).slice
endif

##########

.PHONY: all
all: build

.PHONY: test
test:
	$(TEST_EXEC) ./test/guardian-test $(GUARD_TEST_OPTS)
	$(TEST_CLEAN)
test-verbose:
	$(TEST_EXEC) ./test/guardian-test --verbose $(GUARD_TEST_OPTS)
	$(TEST_CLEAN)

##########

.PHONY: docs
docs:
	$(MAKE) -C docs html
	$(MAKE) -C docs latexpdf

##########

.PHONY: version _release-commit release-major release-minor release-rev release
version:
	echo "__version__ = '$(VERSION)'" >$(VFILE)
_release-commit: test version
	git add $(VFILE)
	git commit -m "Release $(VERSION)"
	git tag -m "Release" $(VERSION)
release-major: VERSION = $(shell PYTHONPATH=lib python3 -m guardian.version -r major $(SVERS))
release-major: _release-commit
release-minor: VERSION = $(shell PYTHONPATH=lib python3 -m guardian.version -r minor $(SVERS))
release-minor: _release-commit
release-rev: VERSION = $(shell PYTHONPATH=lib python3 -m guardian.version -r rev $(SVERS))
release-rev: _release-commit

##########

.PHONY: dist
dist: version
	python3 ./setup.py sdist

.PHONY: build
build: version
	python3 ./setup.py build

.PHONY: install
ifndef PREFIX
install: version
	python3 ./setup.py install
else
install: version
	python3 ./setup.py install --prefix=$(PREFIX)
endif

##################################################

# Debian/Ubuntu test package
.PHONY: deb-snapshot
# FIXME: do this without creating a dist tarball
deb-snapshot: dist
	mkdir -p build
	cd build; tar zxf ../dist/$(DIST).tar.gz
	cp -a debian build/$(DIST)
	cd build/$(DIST); dch -b -v $(SVERS) -D UNRELEASED 'test build, not for upload'
	cd build/$(DIST); echo '3.0 (native)' > debian/source/format
	cd build/$(DIST); debuild -us -uc

.PHONY: deb dsc
deb-src: VERSION = $(shell git describe master)
deb-src: DIST = $(PACKAGE)-$(CVERS)
deb-src:
	rm -rf build/$(VERSION)
	rm -rf build/$(DIST)
	mkdir -p build/$(VERSION)
	git archive master | tar -x -C build/$(VERSION)/
	cd build/$(VERSION) && python3 setup.py sdist
	mv build/$(VERSION)/dist/$(DIST).tar.gz build/$(PACKAGE)_$(VERSION).orig.tar.gz
	cd build && tar xf $(PACKAGE)_$(VERSION).orig.tar.gz
	mkdir build/$(DIST)/debian
	git archive debian:debian | tar -x -C build/$(DIST)/debian/
dsc: VERSION = $(shell git describe master)
dsc: DIST = $(PACKAGE)-$(VERSION)
dsc: deb-src
	cd build/$(DIST) && dpkg-source -b .
deb: VERSION = $(shell git describe master)
deb: DIST = $(PACKAGE)-$(VERSION)
deb: deb-src
	cd build/$(DIST) && debuild -uc -us

##########

.PHONY: clean
clean:
	rm -rf dist
	rm -rf build
	rm -rf test/tmp.*
