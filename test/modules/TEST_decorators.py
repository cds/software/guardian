from guardian import GuardState, GuardStateDecorator

class B(GuardState):
    def main(self):
        return 'B main'

    def run(self):
        return 'B run'

class checkC(GuardStateDecorator):
    def pre_exec(self):
        return 'C pre'

class C(GuardState):
    @checkC
    def main(self):
        return 'C main'
    @checkC
    def run(self):
        return

class checkD(GuardStateDecorator):
    def pre_return_exec(self):
        return 'D pre_return'
    def post_exec(self):
        return 'D post'

class D(GuardState):
    @checkD
    def main(self):
        return 'D main'
    @checkD
    def run(self):
        return

class checkE(GuardStateDecorator):
    def pre_return_exec(self):
        return
    def post_exec(self):
        return 'E post'

class E(GuardState):
    @checkE
    def main(self):
        return 'E main'
    @checkE
    def run(self):
        return
