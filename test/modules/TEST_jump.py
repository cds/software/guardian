from guardian import GuardState

class INIT(GuardState):
    def run(self):
        return 'A'

class Z(GuardState):
    goto = True
    def main(self):
        ezca['A'] = ezca['B'] = ezca['C'] = ezca['D'] = 0

class A(GuardState):
    def main(self):
        self.log('A')
        self.i = 0
        ezca['A'] = self.i

    def run(self):
        if ezca['B'] != 0:
            return 'B'
        if ezca['C'] != 0:
            return 'C'
        if ezca['D'] != 0:
            return 'D'
        if self.i >= 4:
            return True
        else:
            self.i += 1
            ezca['A'] = self.i

class B(GuardState):
    def run(self):
        if ezca['B'] == 0:
            return True

class C(GuardState):
    def run(self):
        if ezca['C'] == 0:
            return True

class D(GuardState):
    redirect = False
    def run(self):
        if ezca['D'] == 0:
            return True

edges = [
    ('Z', 'A'),
    ('B', 'A'),
    ]
