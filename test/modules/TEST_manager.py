from guardian import GuardState
from guardian import Node, NodeManager


node0 = Node('TEST')
nodes1 = NodeManager(['TEST1', 'TEST2'])


class INIT(GuardState):
    def main(self):
        if node0 == 'A':
            return 'C'


class A(GuardState):
    def main(self):
        node0.set_managed()

    @node0.checker(fail_return='FAULT')
    def run(self):
        return True


class B(GuardState):
    request = False
    def main(self):
        node0.set_request('C')


class C(GuardState):
    @node0.checker(fail_return='FAULT')
    def run(self):
        return node0.arrived


class D(GuardState):
    def main(self):
        node0.release()

    @node0.checker()
    def run(self):
        return True


class E(GuardState):
    def main(self):
        nodes1.set_managed(['TEST1'])

    @nodes1.checker(fail_return=False)
    def run(self):
        return True


class FAULT(GuardState):
    request = False
    @node0.checker(fail_return=False)
    def run(self):
        return 'INIT'


edges = [
    ('INIT', 'A'),
    ('INIT', 'B'),
    ('INIT', 'E'),
    ('A', 'B'),
    ('B', 'C'),
    ('C', 'D'),
    ]
