#!/usr/bin/env bash

test_description='system imports'

. lib/test-lib.sh

################################################################

genout() {
    $PYTHON - <<EOF | sed -e "s|$GUARD_MODULE_PATH|MOD_PATH|" -e "s|$TMP_DIRECTORY|TMP_PATH|"
import guardian
sys = guardian.GuardSystem('./module.py')
sys.load()
print(sys.path)
for code in sorted(sys.usercode):
  print(code)
EOF
}

################################################################

test_begin_subtest "module 0"
cat <<EOF >module.py
import foo
EOF
genout >OUTPUT
cat <<EOF >EXPECTED
TMP_PATH/module.py
MOD_PATH/foo.py
EOF
test_expect_equal_file OUTPUT EXPECTED

test_begin_subtest "module 1"
cat <<EOF >module.py
from foo import b
EOF
genout >OUTPUT
cat <<EOF >EXPECTED
TMP_PATH/module.py
MOD_PATH/foo.py
EOF
test_expect_equal_file OUTPUT EXPECTED

test_begin_subtest "package 0"
cat <<EOF >module.py
import bar
EOF
genout >OUTPUT
cat <<EOF >EXPECTED
TMP_PATH/module.py
MOD_PATH/bar/__init__.py
EOF
test_expect_equal_file OUTPUT EXPECTED

test_begin_subtest "package 1"
cat <<EOF >module.py
from bar import a
EOF
genout >OUTPUT
cat <<EOF >EXPECTED
TMP_PATH/module.py
MOD_PATH/bar/__init__.py
EOF
test_expect_equal_file OUTPUT EXPECTED

test_begin_subtest "submodule 0"
cat <<EOF >module.py
import bar.qux
EOF
genout >OUTPUT
cat <<EOF >EXPECTED
TMP_PATH/module.py
MOD_PATH/bar/__init__.py
MOD_PATH/bar/qux.py
EOF
test_expect_equal_file OUTPUT EXPECTED

test_begin_subtest "submodule 1"
cat <<EOF >module.py
from bar import qux
EOF
genout >OUTPUT
cat <<EOF >EXPECTED
TMP_PATH/module.py
MOD_PATH/bar/__init__.py
MOD_PATH/bar/qux.py
EOF
test_expect_equal_file OUTPUT EXPECTED

test_begin_subtest "submodule 2"
cat <<EOF >module.py
from bar.qux import a
EOF
genout >OUTPUT
cat <<EOF >EXPECTED
TMP_PATH/module.py
MOD_PATH/bar/__init__.py
MOD_PATH/bar/qux.py
EOF
test_expect_equal_file OUTPUT EXPECTED

test_begin_subtest "subpackage 0"
cat <<EOF >module.py
import bar.quxx
EOF
genout >OUTPUT
cat <<EOF >EXPECTED
TMP_PATH/module.py
MOD_PATH/bar/__init__.py
MOD_PATH/bar/quxx/__init__.py
EOF
test_expect_equal_file OUTPUT EXPECTED

test_begin_subtest "subpackage 1"
cat <<EOF >module.py
from bar import quxx
EOF
genout >OUTPUT
cat <<EOF >EXPECTED
TMP_PATH/module.py
MOD_PATH/bar/__init__.py
MOD_PATH/bar/quxx/__init__.py
EOF
test_expect_equal_file OUTPUT EXPECTED

test_begin_subtest "subpackage 2"
cat <<EOF >module.py
from bar.quxx import a
EOF
genout >OUTPUT
cat <<EOF >EXPECTED
TMP_PATH/module.py
MOD_PATH/bar/__init__.py
MOD_PATH/bar/quxx/__init__.py
EOF
test_expect_equal_file OUTPUT EXPECTED

test_begin_subtest "subsubmodule 0"
cat <<EOF >module.py
import bar.quxx.z
EOF
genout >OUTPUT
cat <<EOF >EXPECTED
TMP_PATH/module.py
MOD_PATH/bar/__init__.py
MOD_PATH/bar/quxx/__init__.py
MOD_PATH/bar/quxx/z.py
EOF
test_expect_equal_file OUTPUT EXPECTED

test_begin_subtest "subsubmodule 1"
cat <<EOF >module.py
from bar.quxx import z
EOF
genout >OUTPUT
cat <<EOF >EXPECTED
TMP_PATH/module.py
MOD_PATH/bar/__init__.py
MOD_PATH/bar/quxx/__init__.py
MOD_PATH/bar/quxx/z.py
EOF
test_expect_equal_file OUTPUT EXPECTED

test_begin_subtest "subsubmodule 2"
cat <<EOF >module.py
from bar.quxx.z import b
EOF
genout >OUTPUT
cat <<EOF >EXPECTED
TMP_PATH/module.py
MOD_PATH/bar/__init__.py
MOD_PATH/bar/quxx/__init__.py
MOD_PATH/bar/quxx/z.py
EOF
test_expect_equal_file OUTPUT EXPECTED

################################################################

test_done
