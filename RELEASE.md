How to make Guardian releases
=============================

Guardian uses a [semantic versioning](https://semver.org/) scheme,
i.e. `MAJOR.MINOR.REV`, e.g. "15.09.14".

"Rev" releases are reserved mostly for bug fix and minor tweaks that
don't change interfaces or behavior.  "Minor" releases are used for
slightly more substantive changes, particularly if they change any
kind of behavior or interface into or out of the system.  "Major"
releases represent big changes, and are rare.

Released are handled with `make` via the incldue Makefile.  There are
release targets for the three major/minor/rev release possibilities:

* `release-major`: make a new "major" version release
* `release-minor`: make a new "minor" version release
* `release-rev`: make a new "rev" version release

These targets will automatically determine the current overall
version, and will increment the appropriate version for the new
release.  They will also run the full test suite before making a new
version tag, and will abort the release if the tests do not all pass.
Be sure to run the test suite on a system that guardian will
ultimately be running on.  The tests can be run manually with:

```shell
$ make test
```

To make an official release, decide what kind of release is
appropriate for the changes made since the last release (***don't
forget to update the [changelog](NEWS.md)***), and then run the
appropriate make target, e.g.:

```shell
$ make release-minor
```

Once the test suite passes, the release file will be updated and
commited, a new git tag will be made, and the new commits and tags
will be pushed to the official repo.
