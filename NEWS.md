1.5.2
======
* bug fixes

1.5.1
======
* improve MEDM "STATES" screen control interface
* add MEDM button to access code history
* remove default 100 line log limit

1.5.0
======
* add state/index map file to archive

1.4.0
======
* python3

...

1.0.0
======

First release with the new versioning scheme.

* improved guardlog client/server (support node wildcards).
* MEDM control screen improvements
* more informative daemon logging
